const express = require('express');
const router = express.Router();

// @routes  GET /api/profile/test
// @desc    Test profile
// @access  public
router.get('/test',(req,res)=>{
    res.json({
        msg : 'profile worked'
    })
});

module.exports = router;